package com.whereta.dao.impl;

import com.whereta.dao.IRoleDao;
import com.whereta.mapper.RoleMapper;
import com.whereta.model.Role;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Vincent
 * @time 2015/8/27 17:21
 */
@Repository("roleDao")
public class RoleDaoImpl implements IRoleDao {
    @Resource
    private RoleMapper roleMapper;

    /**
     * 根据角色id获取角色
     * @param id
     * @return
     */
    public Role get(List<Role> roles, int id) {
        for(Role role:roles){
            if(role.getId().intValue()==id){
                return role;
            }
        }
        return null;
    }

    /**
     * 获取所有角色
     * @return
     */
    public List<Role> selectAll() {
        return roleMapper.selectAll();
    }

    /**
     * 创建角色
     * @param role
     * @return
     */
    public int createRole(Role role) {
        return roleMapper.insertSelective(role);
    }

    /**
     * 删除角色
     * @param roleId
     * @return
     */
    public int deleteRole(int roleId) {
        return roleMapper.deleteByPrimaryKey(roleId);
    }

    /**
     * 编辑角色
     * @param role
     * @return
     */
    public int updateRole(Role role) {
        return roleMapper.updateByPrimaryKeySelective(role);
    }
}

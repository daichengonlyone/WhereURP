package com.whereta.dao;

import com.whereta.model.Role;

import java.util.List;

/**
 * @author Vincent
 * @time 2015/8/27 17:21
 */
public interface IRoleDao {

    Role get(List<Role> roles, int id);

    List<Role> selectAll();

    int createRole(Role role);

    int deleteRole(int roleId);

    int updateRole(Role role);
}

package com.whereta.model;

import java.io.Serializable;

public class Menu implements Serializable {
    /**
     * menu.id
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private Integer id;

    /**
     * menu.parent_id (菜单上级id)
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private Integer parentId;

    /**
     * menu.name (菜单名字)
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private String name;

    /**
     * menu.url (菜单链接地址)
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private String url;

    /**
     * menu.order (菜单排序)
     * @ibatorgenerated 2015-09-01 10:10:36
     */
    private Integer order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
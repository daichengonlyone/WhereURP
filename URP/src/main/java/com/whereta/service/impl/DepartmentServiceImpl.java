package com.whereta.service.impl;

import com.whereta.dao.IDepartmentAccountDao;
import com.whereta.dao.IDepartmentDao;
import com.whereta.model.Department;
import com.whereta.service.IDepartmentService;
import com.whereta.vo.DepartmentCreateVO;
import com.whereta.vo.DepartmentEditVO;
import com.whereta.vo.ResultVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Vincent
 * @time 2015/9/2 10:15
 */
@Service("departmentService")
public class DepartmentServiceImpl implements IDepartmentService {
    @Resource
    private IDepartmentAccountDao departmentAccountDao;
    @Resource
    private IDepartmentDao departmentDao;

    /**
     * 根据显示的部门id
     *
     * @param userId
     * @return
     */
    public ResultVO getShowDepartments(int userId, Integer rootId,Integer checkUserId) {
        ResultVO resultVO = new ResultVO(true);
        Integer depId = departmentAccountDao.getDepIdByAccountId(userId);
        if (depId == null) {
            resultVO.setOk(false);
            resultVO.setMsg("用户部门不存在");
            return resultVO;
        }
        //获取所有部门
        List<Department> departments = departmentDao.selectAll();
        //获取部门
        Department department = departmentDao.get(departments, depId);
        if (department == null) {
            resultVO.setOk(false);
            resultVO.setMsg("用户部门不存在");
            return resultVO;
        }

        Set<Integer> childrenIdSet=null;
        if(rootId!=null){
            childrenIdSet=getChildrenDepIds(departments,rootId);
            childrenIdSet.add(rootId);
        }

        //获取用户部门id
        Set<Integer> checkDepIds=null;
        if(checkUserId!=null) {
            checkDepIds=new HashSet<>();
            Integer depIdByAccountId = departmentAccountDao.getDepIdByAccountId(checkUserId);
            if(depIdByAccountId!=null) {
                checkDepIds.add(depIdByAccountId);
            }
        }

        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", department.getId());
        map.put("text", department.getName());
        map.put("order", department.getOrder());
        if(checkDepIds!=null&&checkDepIds.contains(department.getId().intValue())){
            map.put("checked", true);
        }else{
            map.put("checked", false);
        }
        map.put("children", getChildrenDeps(departments, department.getId(),childrenIdSet,checkDepIds));
        mapList.add(map);

        resultVO.setData(mapList);

        return resultVO;
    }

    /**
     * 创建部门
     *
     * @param createVO
     * @return
     */
    public ResultVO createDepartment(DepartmentCreateVO createVO) {
        ResultVO resultVO = new ResultVO(true);
        Integer parentId = createVO.getParentId();
        if (parentId != null) {
            //获取所有部门
            List<Department> departments = departmentDao.selectAll();
            //获取部门
            Department department = departmentDao.get(departments, parentId);
            if (department == null) {
                resultVO.setOk(false);
                resultVO.setMsg("上级部门不存在");
                return resultVO;
            }
        }
        Department department = new Department();
        department.setParentId(createVO.getParentId());
        department.setName(createVO.getName());
        department.setOrder(createVO.getOrder());
        departmentDao.create(department);
        resultVO.setMsg("创建部门成功");
        return resultVO;
    }

    /**
     * 编辑部门
     *
     * @param editVO
     * @return
     */
    public ResultVO editDepartment(DepartmentEditVO editVO, int accountId) {
        ResultVO resultVO = new ResultVO(true);
        Integer parentId = editVO.getParentId();
        //获取所有部门
        List<Department> departments = departmentDao.selectAll();
        if (parentId != null) {
            //获取部门
            Department department = departmentDao.get(departments, parentId);
            if (department == null) {
                resultVO.setOk(false);
                resultVO.setMsg("上级部门不存在");
                return resultVO;
            }
        }
        Department myDepartment = departmentDao.get(departments, editVO.getId());
        if(myDepartment==null){
            resultVO.setOk(false);
            resultVO.setMsg("部门不存在");
            return resultVO;
        }
        //判断是否是自己的部门
        Integer myDepId = departmentAccountDao.getDepIdByAccountId(accountId);
        if (myDepId == null) {
            resultVO.setOk(false);
            resultVO.setMsg("用户部门不存在");
            return resultVO;
        }
        if (myDepId != null && myDepId.intValue() == editVO.getId().intValue()) {
            resultVO.setOk(false);
            resultVO.setMsg("您所在部门不能编辑");
            return resultVO;
        }
        //上级id是否在自己所在部门或者下级
        Set<Integer> childrenDepIds = getChildrenDepIds(departments, editVO.getId());
        childrenDepIds.add(editVO.getId());

        if(childrenDepIds.contains(editVO.getParentId())){
            resultVO.setOk(false);
            resultVO.setMsg("所在部门的上级不能为自己所在部门或者下级部门");
            return resultVO;
        }

        Department department = new Department();
        department.setId(editVO.getId());
        department.setName(editVO.getName());
        department.setOrder(editVO.getOrder());
        department.setParentId(editVO.getParentId());
        departmentDao.update(department);
        resultVO.setMsg("编辑部门成功");
        return resultVO;
    }

    /**
     * 删除部门
     *
     * @param depId
     * @param accountId
     * @return
     */
    public ResultVO deleteDep(int depId, int accountId) {
        ResultVO resultVO = new ResultVO(true);
        //获取所有部门
        List<Department> departments = departmentDao.selectAll();
        //获取部门
        Department department = departmentDao.get(departments, depId);
        if (department == null) {
            resultVO.setOk(false);
            resultVO.setMsg("部门不存在");
            return resultVO;
        }
        //判断是否是自己的部门
        Integer myDepId = departmentAccountDao.getDepIdByAccountId(accountId);
        if (myDepId != null && myDepId.intValue() == depId) {
            resultVO.setOk(false);
            resultVO.setMsg("不能删除自己所在部门");
            return resultVO;
        }
        //获取子级部门id
        Set<Integer> childrenDepIds = getChildrenDepIds(departments, depId);
        //删除
        departmentDao.delete(depId);
        departmentAccountDao.deleteByDepId(depId);
        for (Integer id : childrenDepIds) {
            departmentDao.delete(id);
            departmentAccountDao.deleteByDepId(id);
        }
        resultVO.setMsg("删除部门成功");
        return resultVO;
    }

    public static Set<Integer> getChildrenDepIds(List<Department> list, Integer depId) {
        Set<Integer> depIdList = new HashSet<Integer>();
        for (Department department : list) {
            Integer departmentId = department.getParentId();
            if ((departmentId == null && depId == null) || (departmentId != null && depId != null && departmentId.intValue() == depId.intValue())) {
                depIdList.add(department.getId());
                depIdList.addAll(getChildrenDepIds(list, department.getId()));
            }
        }
        return depIdList;
    }

    /**
     * 获取子级部门
     *
     * @param list
     * @param depId
     * @return
     */
    public static List<Map<String, Object>> getChildrenDeps(List<Department> list, Integer depId, Set<Integer> chidlrenDepIdSet,Set<Integer> checkedIdSet) {
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();

        for (Department department : list) {
            Integer departmentParentId = department.getParentId();
            if ((departmentParentId == null && depId == null) || (departmentParentId != null && depId != null && departmentParentId.intValue() == depId.intValue())) {
                if (chidlrenDepIdSet == null || !chidlrenDepIdSet.contains(department.getId().intValue())) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("id", department.getId());
                    map.put("text", department.getName());
                    map.put("order", department.getOrder());
                    if(checkedIdSet!=null&&checkedIdSet.contains(department.getId().intValue())){
                        map.put("checked", true);
                   }else{
                        map.put("checked", false);
                    }
                    map.put("children", getChildrenDeps(list, department.getId(), chidlrenDepIdSet, checkedIdSet));
                    mapList.add(map);
                }
            }
        }

        return mapList;
    }
}

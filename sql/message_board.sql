SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `message_board`;
CREATE TABLE `message_board` (
  id            INT PRIMARY KEY AUTO_INCREMENT,
  user_id       INT      NOT NULL COMMENT '留言用户id',
  msg           TEXT     NOT NULL COMMENT '留言内容',
  create_time   DATETIME NOT NULL COMMENT '留言时间',
  parent_id     INT      NULL COMMENT '上级id',
  reply_time    DATETIME NULL COMMENT '回复时间',
  reply_msg     TEXT     NULL COMMENT '回复内容',
  reply_user_id INT      NULL COMMENT '回复人id'
)ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
